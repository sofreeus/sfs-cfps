# Every Change is an Experiment: Iterating on the Intro to DevOps Class
## Abstract

The Software Freedom School walks through the iteration and improvement of their Intro to DevOps class. 
From its beginnings as a public repository on Gitlab, SFS has tweaked content, scope and delivery of the class. 
In this talk, the team will show the journey of experimentation and fast iteration that allowed them to rapidly respond to class feedback.  

## Notes

# Template

## [ 0:00 - 4:00 ] Introductions & Bio
  - Class Framework: The Time Pie
  - Who are our students

## [ 4:00 -  7:00 ] Problem Statement
  - Originally conceived by Aaron Brown to answer the question "What do I do everyday as a DevOps Engineer"
  - Limited Data, Every deploy is an experiment
    - How do we collect data? 
  - Level-setting. Can we cover Linux, Bash, Git, CI, Docker, AWS,  GCP, and K8s for a broad audience

## [ 7:00 - 14:00 ] First Iterations

[ 7:00 - 11:00 ] Initial Commit
  - Originally conceived as separate, 4-hour classes
  - Public Repo
  - Individually-scoped
  - SFS Method: Hands-on Labs.

[ 11:00 - 13:00 ] Prototyping in-house
  - Manual QA from SFS board. Catching bugs, cleaning up language
    - The call for better data

[ 13:00 - 14:00 ] Pluggable Modules

## [ 14:00 - 21:00 ] What about a Camp?

[ 14:00 - 17:00 ] Refactoring for a 5-day intensive
  - Previous Camps
  - Changing scope, again

[ 17:00 - 21:00 ] The First DevOps Camp


## [ 21:00 - 28:00 ] The One-Day Class

[ 21:00 - 23:00 ] The First Delivery
  - Experimenting w/ PWYC, Scholarhsips

[ 23:00 - 25:00 ] What We Learned

[ 25:00 - 27:00 ] Re-adding Kubernetes

[ 27:00 - 29:00 ] Customization for the Audience
  - CU Denver Custom Class

## [ 29:00 - 30:00 ] Closings and Thanks
